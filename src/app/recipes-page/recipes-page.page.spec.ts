import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RecipesPagePage } from './recipes-page.page';

describe('RecipesPagePage', () => {
  let component: RecipesPagePage;
  let fixture: ComponentFixture<RecipesPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipesPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RecipesPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
