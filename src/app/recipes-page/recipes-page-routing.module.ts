import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecipesPagePage } from './recipes-page.page';

const routes: Routes = [
  {
    path: '',
    component: RecipesPagePage
  },
  {
    path: ':recipeId',
    loadChildren: () => import('./recipes-detail/recipes-detail.module').then( m => m.RecipesDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecipesPagePageRoutingModule {}
